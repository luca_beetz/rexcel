# frozen_string_literal: true

require 'erb'
require 'fileutils'
require 'rubyXL/convenience_methods'
require_relative 'path_creator'

# This class takes in two worksheets and compares them
class WorksheetDiffer
  attr_reader :amount_of_total_cells
  attr_reader :differing_cells

  # Create a new instance of WorksheetDiffer
  #
  # @param worksheet_a [RubyXL::Worksheet] - Worksheet of file A
  # @param worksheet_b [RubyXL::Worksheet] - Worksheet of file B
  # @param options [Hash] - Options needed for the script to run, like the output dir
  def initialize(worksheet_a, worksheet_b, options)
    @worksheet_a = worksheet_a
    @worksheet_b = worksheet_b

    @options = options

    @differing_cells = []
    @amount_of_total_cells = 0

    # Create the output directory
    FileUtils.mkdir_p(@options[:output_dir])
  end

  # Prints info about both worksheets and their differences
  def print_info
    print_info_about_worksheets

    # Print a list of different cells
    puts '----- Different cells -----'
    @differing_cells.each do |cells|
      row_index = cells[:cell_a].row
      col_index = cells[:cell_a].column
      puts "Cells at row: #{row_index + 1} and col: #{col_index + 1} differ"
    end
  end

  # Check whether tables can be compared
  #
  # @return [bool] - Whether tables can be compared
  def tables_are_valid
    # Return false if the tables don't have the same number of rows
    if @worksheet_a.sheet_data.size != @worksheet_b.sheet_data.size
      puts "The tables differ in the number of rows, can't compare them"
      return false
    end

    # Return false if table has no rows
    if @worksheet_a.sheet_data.size <= 0
      puts 'The tables are empty'
      return false
    end

    # Return false if the tables don't have the same number of columns
    if @worksheet_a.sheet_data[0].size != @worksheet_b.sheet_data[0].size
      puts "The tables differ in the number of columns, can't compare them"
      return false
    end

    true
  end

  # Loop through the sheets and create an array of different cells
  def calculate_diff
    # Calculate the total amount of cells
    @amount_of_total_cells = @worksheet_a.sheet_data.size * @worksheet_a.sheet_data[0].size

    # Loop through each cell
    (0..@worksheet_a.sheet_data.size - 1).each do |row_index|
      (0..@worksheet_a.sheet_data[row_index].size - 1).each do |col_index|
        # Get cells of file A and B
        cell_a = get_cell_and_add_if_nil(@worksheet_a, row_index, col_index)
        cell_b = get_cell_and_add_if_nil(@worksheet_b, row_index, col_index)

        # Skip this iteration if the cells are equal
        next if cell_a == cell_b

        # Add cells to list of differing cells
        @differing_cells.push(
          cell_a: cell_a,
          cell_b: cell_b
        )
      end
    end
  end

  # Write information about the files to a .txt file
  def write_info_to_file
    template = File.read(File.join(File.dirname(__FILE__), 'diff_table.erb'))

    # Create name of output .txt file (with version information if possible)
    output_file_path = PathCreator.create_simple_path(
      @options[:file_path_a],
      @options[:file_path_b],
      @options[:output_dir],
      'txt'
    )

    File.open(output_file_path, 'w') do |file|
      file.write(ERB.new(template, nil, '-').result(binding))
    end
  end

  # Copy one of the tables and mark all the differences in the sheets
  def write_changes_to_sheet
    # Create output file name (with version information if possible)
    output_file_path = PathCreator.create_simple_path(
      @options[:file_path_a],
      @options[:file_path_b],
      @options[:output_dir],
      'xlsx'
    )

    # Copy the file
    FileUtils.cp(@options[:file_path_b], output_file_path)

    # Parse the copied workbook
    workbook_diff = RubyXL::Parser.parse(output_file_path)
    worksheet_diff = workbook_diff[@options[:sheet_name]]

    vals = RubyXL::DataValidations.new
    worksheet_diff.data_validations = vals

    mark_diff_cells_in_worksheet(worksheet_diff)

    # Save changes
    workbook_diff.write(output_file_path)
  end

  # Private methods only needed by the other public methods inside of this class
  private

  # Mark all different cells in the sheet
  #
  # @param worksheet_diff [RubyXL::Worksheet] - Worksheet to write to
  def mark_diff_cells_in_worksheet(worksheet_diff)
    @differing_cells.each do |cells|
      cell = cells[:cell_a]

      # Add cells which didn't exist in file B to the output sheet
      get_cell_and_add_if_nil(worksheet_diff, cell.row, cell.column)

      highlight_cell(worksheet_diff, cell)
      comment_cell(worksheet_diff, cells)
    end
  end

  # Get a cell from the worksheet and add an empty one if it doesn't exist
  #
  # @param worksheet [RubyXL::Worksheet] - Worksheet containing the cell
  # @param row_index [Int] - Row of the cell
  # @param col_index [Int] - Column of the cell
  #
  # @return [RubyXL::Cell] The cell
  def get_cell_and_add_if_nil(worksheet, row_index, col_index)
    cell_or_nil = worksheet.sheet_data[row_index][col_index]

    if cell_or_nil.nil?
      worksheet.add_cell(row_index, col_index, '')
      return worksheet.sheet_data[row_index][col_index]
    end

    cell_or_nil
  end

  # Highlight the given cell in the worksheet
  #
  # @param worksheet [RubyXL::Worksheet] - The worksheet to be edited
  # @param cell [RubyXL::Cell] - Cell to highlight
  def highlight_cell(worksheet, cell)
    worksheet.sheet_data[cell.row][cell.column].change_fill('11d1c2')
    worksheet.sheet_data[cell.row][cell.column].change_font_bold(true)
    worksheet.sheet_data[cell.row][cell.column].change_font_color('FFFFFF')
  end

  # Add a comment to the cell describing the differences
  #
  # @param worksheet [RubyXL::Worksheet] - The worksheet to be edited
  # @param cells [Hash] - Hash containing cell of file A and B
  def comment_cell(worksheet, cells)
    # Create a description of the differences
    diff_desc = "Value file A: #{cells[:cell_a].value}\nValue file B: #{cells[:cell_b].value}"

    # Add a description of further differences
    diff_desc += get_cell_differences(cells[:cell_a], cells[:cell_b])

    cell = cells[:cell_a]

    # Add comment to the cell
    worksheet.add_hint(cell.row, cell.column, nil, 'Changes in cell', diff_desc)
  end

  # Print general info about the worksheets
  def print_info_about_worksheets
    # Percentage by how much both sheets differ
    differ_percentage = (@differing_cells.size.to_f / @amount_of_total_cells) * 100

    # Print general info about the sheets
    puts '----- General info -----'
    puts "File A: Rows: #{@worksheet_a.sheet_data.size} | Cols: #{@worksheet_a.sheet_data[0].size}"
    puts "File B: Rows: #{@worksheet_b.sheet_data.size} | Cols: #{@worksheet_b.sheet_data[0].size}"
    puts ''

    # Print info about cells and differences
    puts '----- Stats -----'
    puts "Total amount of cells: #{@amount_of_total_cells}"
    puts "Amount of different cells: #{@differing_cells.size}"
    puts "The two sheets differ by #{format('%.2f', differ_percentage)}%"
    puts ''
  end

  # Get a String of differences between the cells
  #
  # @param cell_a [RubyXL::Cell] Cell of file A
  # @param cell_b [RubyXL::Cell] Cell of file B
  #
  # @return [String] Description of the differences
  def get_cell_differences(cell_a, cell_b)
    desc  = "\n"
    desc += "\nThe fill color has changed" if cell_a.fill_color != cell_b.fill_color
    desc += "\nThe font color has changed" if cell_a.font_color != cell_b.font_color
    desc += "\nThe font name has changed" if cell_a.font_name != cell_b.font_name
    desc += "\nThe font size has changed" if cell_a.font_size != cell_b.font_size

    desc
  end

  # Create an empty cell
  #
  # @param row [Int] - Row of cell
  # @param col [Int] - Column of cell
  #
  # @return [RubyXL::Cell] - New empty cell
  def create_empty_cell(row, col)
    cell = RubyXL::Cell.new
    cell.row = row
    cell.column = col
    cell.raw_value = ''

    cell
  end
end

# Helper method for adding comment like hints using RubyXL
# See issue: https://github.com/weshatheleopard/rubyXL/issues/224
module RubyXL
  # Adds a convenience method to add hints to cells
  module WorksheetConvenienceMethods
    def add_dropdown(row, col, content_list = nil, title = nil, prompt = nil)
      formula = RubyXL::Formula.new(expression: content_list)
      loc = if content_list # Indicates it is a dropdown.
              RubyXL::Reference.new(row, 1_048_000, col, col)
            else
              RubyXL::Reference.new(row, col)
            end
      val = RubyXL::DataValidation.new(
        prompt_title: title,
        prompt: prompt,
        sqref: loc,
        formula1: content_list ? formula : nil,
        type: content_list ? 'list' : nil,
        show_input_message: true
      )
      data_validations << val
    end
    alias add_hint add_dropdown # Alias as to not confuse myself
  end
end
