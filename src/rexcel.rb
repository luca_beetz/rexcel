# frozen_string_literal: true

require 'rubyXL'
require 'optparse'
require_relative 'worksheet_differ'

# Get command line arguments
options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: rexcel [options]'
  opts.on('-a', '--file-a <FILE_PATH>', String, 'Path to file A') do |file_name|
    options[:file_path_a] = file_name
  end
  opts.on('-b', '--file-b <FILE_PATH>', String, 'Path to file B') do |file_name|
    options[:file_path_b] = file_name
  end
  opts.on('-s', '--sheet <SHEET_NAME>', String, 'Name of the sheet to compare') do |sheet_name|
    options[:sheet_name] = sheet_name
  end
  opts.on('-o', '--output <dir>', String, 'Output file name (optional)') do |output_dir|
    options[:output_dir] = output_dir
  end
end.parse!

# Options parameter is optional
options[:output_dir] ||= '.'

# Name of worksheets to compare
worksheet_name = options[:sheet_name]

# Parse both files into workbooks
workbook_a = RubyXL::Parser.parse(options[:file_path_a])
workbook_b = RubyXL::Parser.parse(options[:file_path_b])

# Create a new instance of WorksheetDiffer
worksheet_differ = WorksheetDiffer.new(
  workbook_a[worksheet_name],
  workbook_b[worksheet_name],
  options
)

# Make sure the tables can be compared
if worksheet_differ.tables_are_valid
  worksheet_differ.calculate_diff
  worksheet_differ.print_info
  worksheet_differ.write_info_to_file
  worksheet_differ.write_changes_to_sheet
end
