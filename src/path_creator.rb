# frozen_string_literal: true

# A utility class for generating paths and file names
class PathCreator
  # Creates a simple name by concatenating the names of both input files with a 'TO' between them
  #
  # @param file_name_a [String] - Name of file A (also accepts path)
  # @param file_name_b [String] - Name of file B (also accepts path)
  # @param directory [String] - Directory to append path to
  # @param extension [String] - File extension
  #
  # @return [String] Created file name
  def self.create_simple_path(file_name_a, file_name_b, directory, extension)
    file_extension_a = File.extname(file_name_a)
    file_extension_b = File.extname(file_name_b)

    file_a_basename = File.basename(file_name_a, file_extension_a)

    # Try reading the version from the file name
    parsed_version = get_version_of_name(file_name_b)
    if parsed_version == file_name_b
      # Can't parse version, use file name instead
      parsed_version = File.basename(file_name_b, file_extension_b)
    end

    output_file_name = "#{file_a_basename}_TO_#{parsed_version}.output_diff.#{extension}"

    File.join(directory, output_file_name)
  end

  # Read the version of a file_name using a regular expression
  #
  # @param file_name [String] - Name of the file
  #
  # @result [String] - Either the version or the full file name if the regex didn't match
  def self.get_version_of_name(file_name)
    match_data = file_name.match(/[V](\d+[.])+\d+\w*/)

    # Return version or the file_name if the regex didn't match
    defined?(match_data[0]) ? match_data[0] : file_name
  end
end
