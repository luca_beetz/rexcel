# Rexcel

![Build Status](https://gitlab.com/luca_beetz/rexcel/badges/master/build.svg)

[![coverage report](https://gitlab.com/luca_beetz/rexcel/badges/master/coverage.svg)](https://gitlab.com/luca_beetz/rexcel/commits/master)

Rexcel is a tool which compares two excel `.xlsx` sheets and produces both statistics about
the differences as well as a new excel sheet with the differences highlighted.

## Using the tool

Rexcel is primarily thought to be used as a small script. To use it run the `rexcel.rb` file as follows:
```
ruby rexcel.rb -a <PATH_TO_FILE_A> -b <PATH_TO_FILE_B> -s <SHEET_NAME> -o <output_dir>
```

The arguments `PATH_TO_FILE_A`, `PATH_TO_FILE_B` and `SHEET_NAME` are required. The `output_dir` argument
is optional. By default the two output files are generated in the directory where the command was run.

### Output

The name of the output files is created after the following scheme: `"#{file_name_a}_TO_{file_name_b}"`.

If the file name contains a version (e.g `some_file_V1.7a.txt`) the output name looks like this: `"#{file_name_a}_TO_#{version_of_file_b}"`

The version is parsed by matching `<file_name_b>` against the following regex `/[V](\d+[.])+\d+\w*/`.
(You can test it here: https://regex101.com/r/X8UleT/1)