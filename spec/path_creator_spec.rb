# frozen_string_literal: true

require 'spec_helper'
require_relative '../src/path_creator'

describe PathCreator do
  let(:file_name_a) { 'test_file_a_V1.3.txt' }
  let(:file_name_b) { 'test_file_b_V1.7.txt' }
  let(:output_dir) { 'output/test/dir' }
  let(:extension) { 'xlsx' }

  it 'creates path out of two file names and appends it to the given directory' do
    created_path = PathCreator.create_simple_path(
      file_name_a,
      file_name_b,
      output_dir,
      extension
    )

    desired_path = File.join(output_dir, 'test_file_a_V1.3_TO_V1.7.output_diff.xlsx')

    expect(created_path).to eq(desired_path)
  end

  it 'reads version of file name' do
    version_a = PathCreator.get_version_of_name(file_name_a)
    expect(version_a).to eq('V1.3')

    version_b = PathCreator.get_version_of_name(file_name_b)
    expect(version_b).to eq('V1.7')

    no_version = PathCreator.get_version_of_name('this_string_has_no_V.rsion_info12.4_in_it')
    expect(no_version).to eq('this_string_has_no_V.rsion_info12.4_in_it')
  end
end
