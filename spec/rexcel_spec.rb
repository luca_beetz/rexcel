# frozen_string_literal: true

require 'spec_helper'
require 'rubyXL'
require_relative '../src/worksheet_differ'
require_relative '../src/path_creator'

describe WorksheetDiffer do
  let(:options) do
    {
      file_path_a: 'spec/input_files/test_file_a.xlsx',
      file_path_b: 'spec/input_files/test_file_b.xlsx',
      sheet_name: 'Sheet1',
      output_dir: 'spec/output_files'
    }
  end
  let(:workbook_a) { RubyXL::Parser.parse(options[:file_path_a]) }
  let(:workbook_b) { RubyXL::Parser.parse(options[:file_path_b]) }
  let(:worksheet_differ) do
    WorksheetDiffer.new(
      workbook_a[options[:sheet_name]],
      workbook_b[options[:sheet_name]],
      options
    )
  end

  # Tests whether WorksheetDiffer can successfully compare two sheets and gather information about
  # them like the total amount of cells as well as how many of them differ between the two tables
  it 'compares worksheets and stores different cells' do
    expect(worksheet_differ.tables_are_valid).to be true

    worksheet_differ.calculate_diff

    expect(worksheet_differ.amount_of_total_cells).to eq(30)
    expect(worksheet_differ.differing_cells.size).to eq(13)
  end

  # Tests whether WorksheetDiffer creates both the output .txt as well as the output .xlsx files
  it 'creates output files' do
    expect(worksheet_differ.tables_are_valid).to be true

    worksheet_differ.calculate_diff

    # Write to new .txt file and test whether it exists
    worksheet_differ.write_info_to_file

    output_file_path_txt = PathCreator.create_simple_path(
      options[:file_path_a],
      options[:file_path_b],
      options[:output_dir],
      'txt'
    )
    expect(File.exist?(output_file_path_txt)).to be true

    # Write to new .xlsx file and test whether it exists
    worksheet_differ.write_changes_to_sheet

    output_file_path_xlsx = PathCreator.create_simple_path(
      options[:file_path_a],
      options[:file_path_b],
      options[:output_dir],
      'xlsx'
    )
    expect(File.exist?(output_file_path_xlsx)).to be true
  end
end
